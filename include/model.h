#ifndef MODEL_H
#define MODEL_H

enum BlockState{
    INIT, // Initial state: undigged
    DIGGED,
    MARKED,
    BOMB,
    WRONG_MARK
};

class MineBlock{
public:
    MineBlock():state(INIT),value(0){}
    BlockState getState();
    int getValue();
    void setState(BlockState);
    void setValue(int);
private:
    BlockState state;
    int value; // 0-8 with -1 for boom
};

enum GameState{
    PLAYING,
    LOSE,
    OVER,
    WIN
};

// Default configuration
const int defRow = 15;
const int defCol = 20;
const int defMineCount = 50;
const int defTime = 0;

#endif // MODEL_H
