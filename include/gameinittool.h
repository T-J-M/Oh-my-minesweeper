#ifndef GAMEINITTOOL_H
#define GAMEINITTOOL_H

#include "model.h"
#include "gamemodel.h"
#include <vector>

namespace GameInitTool{
    void InitMineBlocks(BlockMap & map, int row, int col);
    void ArrangeMine(BlockMap & map, int row, int col, int totalNum);
    void CalBlockSurrond(BlockMap & map, int row, int col);
};

#endif // GAMEINITTOOL_H
