#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include "model.h"
#include <vector>

typedef std::vector<std::vector<MineBlock>> BlockMap;

class GameModel
{
public:
    GameModel();
    ~GameModel();
public:
    void dig(int x, int y);
    void mark(int x, int y);
    void createGame(
            int row = defRow,
            int col = defCol,
            int mineCount = defMineCount
       );
    void restartGame();
    void checkGameState();
    int getMapCol();
    int getMapRow();
    int getTimer();
    GameState getState();
    int getCurrentMineNum();
    BlockMap& getMap();
    void updateTimer();
private:
    BlockMap gameMap; // The game map
    int mapRow;
    int mapCol;
    int totalMineNum;
    int currMineNum;
    int timer; // This is in seconds
    GameState gameState;
};

#endif // GAMEMODEL_H
