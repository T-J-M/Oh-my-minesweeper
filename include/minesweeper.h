#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "gamemodel.h"

#ifndef UNUSED
#define UNUSED(X) (void)(X);
#endif

namespace Ui {
class MineSweeper;
}

class MineSweeper : public QMainWindow
{
    Q_OBJECT

public:
    explicit MineSweeper(QWidget *parent = nullptr);
    ~MineSweeper();

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent* event);

private:
    Ui::MineSweeper *ui;

    GameModel * game;
    QTimer * timer;
    QLabel * timelabel;

    void handle(GameModel * game);
private slots:
    void onStartGameClicked();
    void updateTimer();
};

#endif // MAINWINDOW_H
