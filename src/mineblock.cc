#include "model.h"

BlockState MineBlock::getState(){
    return this->state;
}

int MineBlock::getValue(){
    return this->value;
}

void MineBlock::setState(BlockState state){
    this->state = state;
}

void MineBlock::setValue(int value){
    this->value = value;
}
