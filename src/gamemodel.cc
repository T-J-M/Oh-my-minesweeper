#include "gamemodel.h"
#include "gameinittool.h"
#include "model.h"

GameModel::GameModel():
    mapRow(defRow),
    mapCol(defCol),
    totalMineNum(defMineCount),
    timer(defTime),
    gameState(PLAYING){
}

GameModel::~GameModel(){
}

void GameModel::createGame(int row, int col, int mineCount){
    // Clean exist map
    this->gameMap.clear();
    this->mapRow = row;
    this->mapCol = col;
    this->totalMineNum = mineCount;
    this->currMineNum = this->totalMineNum;
    this->gameState = PLAYING;
    this->timer = 0;
    BlockMap & map = this->getMap();
    GameInitTool::InitMineBlocks(map, this->mapRow, this->mapCol);
    GameInitTool::ArrangeMine(map, this->mapRow, this->mapCol, this->totalMineNum);
    GameInitTool::CalBlockSurrond(map, this->mapRow, this->mapCol);
}

void GameModel::dig(int m, int n){
    unsigned long x = static_cast<unsigned long>(m);
    unsigned long y = static_cast<unsigned long>(n);
    MineBlock& block = gameMap[x][y];

    // Normal block
    if(block.getValue()>0
     && INIT==block.getState()){
        block.setState(DIGGED);
    }

    // Untouched block
    if(0 == block.getValue()
            && INIT == block.getState()){
        block.setState(DIGGED);
        for(int x = -1; x < 2; x++){
            for(int y = -1; y < 2; y++){
                int newM = m + x;
                int newN = n + y;
                if(newM>=0
                  && newM<this->mapRow
                  && newN>=0
                  && newN<this->mapCol
                        && !(0==x&&0==y)){
                    this->dig(newM,newN);
                }
            }
        }
    }

    // Hit the boom
    if(-1==block.getValue()){
        this->gameState = OVER;
        block.setState(BOMB);
    }

    // Check Game State
    this->checkGameState();
}

void GameModel::mark(int m, int n){
    unsigned long x = static_cast<unsigned long>(m);
    unsigned long y = static_cast<unsigned long>(n);

    MineBlock& block = gameMap[x][y];
    if(block.getState()==INIT){
        if(block.getValue()==-1){
            block.setState(MARKED);
        } else {
            this->gameState = LOSE;
            block.setState(WRONG_MARK);
        }
        this->currMineNum--;
    } else if(block.getState()==MARKED||block.getState()==WRONG_MARK){
        block.setState(INIT);
        this->gameState = PLAYING;
        this->currMineNum++;
    }

    this->checkGameState();
}

void GameModel::checkGameState(){
    if(this->gameState==OVER){
        for(int i=0; i<this->mapRow; i++){
            for(int j=0; j<this->mapCol; j++){
                unsigned long x = static_cast<unsigned long>(i);
                unsigned long y = static_cast<unsigned long>(j);
                MineBlock& block = this->gameMap[x][y];
                if(block.getValue()==-1){
                    block.setState(BOMB);
                }
            }
        }
        return;
    }
    if(this->gameState!=LOSE){
        for(int i=0; i<this->mapRow; i++){
            for(int j=0; j<this->mapCol; j++){
                unsigned long x = static_cast<unsigned long>(i);
                unsigned long y = static_cast<unsigned long>(j);
                MineBlock& block = this->gameMap[x][y];
                if(block.getState()==INIT){
                    this->gameState=PLAYING;
                    return;
                }
            }
        }
        this->gameState = WIN;
    }
}

int GameModel::getMapCol(){
    return this->mapCol;
}

int GameModel::getMapRow(){
    return this->mapRow;
}

int GameModel::getTimer(){
    return this->timer;
}

void GameModel::updateTimer(){
    this->timer++;
}

GameState GameModel::getState(){
    return this->gameState;
}

int GameModel::getCurrentMineNum(){
    return this->currMineNum;
}

BlockMap& GameModel::getMap(){
    return this->gameMap;
}

void GameModel::restartGame(){
    this->createGame(mapRow,mapCol,this->totalMineNum);
}
