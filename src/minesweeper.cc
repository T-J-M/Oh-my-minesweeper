#include <QMenu>
#include <QTimer>
#include <QPainter>
#include <QPixmap>
#include <QMouseEvent>
#include "gamemodel.h"
#include "minesweeper.h"
#include <ui_minesweeper.h>

const int block_size = 20;
const int offsetX = 5;
const int offsetY = 5;
const int spaceY = 70;

MineSweeper::MineSweeper(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MineSweeper)
{
    ui->setupUi(this);
    this->timelabel = new QLabel(this);

    connect(ui->actionStart,SIGNAL(triggered(bool)),this,SLOT(onStartGameClicked()));
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(updateTimer()));

    this->game = new GameModel();
    this->game->createGame();
    int mapCol = this->game->getMapCol();
    int mapRow = this->game->getMapRow();

    setFixedSize(mapCol*block_size+offsetX*2,mapRow*block_size+offsetY*2+spaceY);
    this->timelabel->setGeometry(mapCol*block_size+offsetX*2-80,spaceY/2,80,20);
    this->timelabel->setText("Time:"+QString::number(this->game->getTimer()));
    timer->start(1000);
}

MineSweeper::~MineSweeper()
{
    delete this->game;
    this->game = nullptr;
    delete ui;
}

void MineSweeper::paintEvent(QPaintEvent * event){
    UNUSED(event);
    QPainter painter(this);
    QPixmap bmpBlocks(":/icons/blocks.bmp");
    QPixmap bmpFaces(":/icons/faces.bmp");
    QPixmap bmpFrame(":/icons/frame.bmp");
    QPixmap bmpNumber(":/icons/timenumber.bmp");

    int mapCol = this->game->getMapCol();
    int mapRow = this->game->getMapRow();

    switch(this->game->getState()){
    case OVER:
        painter.drawPixmap((mapCol * block_size + offsetX * 2) / 2 - 12, spaceY / 2, bmpFaces, 0 * 24, 0, 24, 24);
        break;
    case PLAYING:
        painter.drawPixmap((mapCol * block_size + offsetX * 2) / 2 - 12, spaceY / 2, bmpFaces, 1 * 24, 0, 24, 24);
        break;
    case WIN:
        painter.drawPixmap((mapCol * block_size + offsetX * 2) / 2 - 12, spaceY / 2, bmpFaces, 2 * 24, 0, 24, 24);
        break;
    default:
        painter.drawPixmap((mapCol * block_size + offsetX * 2) / 2 - 12, spaceY / 2, bmpFaces, 1 * 24, 0, 24, 24);
        break;
    }

    int n = this->game->getCurrentMineNum();
    int posX = (mapCol * block_size + offsetX * 2) / 2 - 50;
    if(n<=0){
         painter.drawPixmap(posX, spaceY / 2, bmpNumber, n * 20, 0, 20, 28);
    }
    while(n>0){
        painter.drawPixmap(posX - 20, spaceY / 2, bmpNumber, n % 10 * 20, 0, 20, 28);
        n /= 10;
        posX -= 20;
    }

    BlockMap& map = this->game->getMap();

    for(int i = 0; i<mapRow; i++){
        for(int j =0; j<mapCol; j++){
            unsigned long x = static_cast<unsigned long>(i);
            unsigned long y = static_cast<unsigned long>(j);
            MineBlock& block = map[x][y];
            GameState state = this->game->getState();
            switch(block.getState()){
                case INIT:
                    painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY ,
                                       bmpBlocks, block_size * 10, 0, block_size, block_size);
                    break;
                case DIGGED:
                    painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY,
                                       bmpBlocks, block_size * block.getValue(), 0, block_size, block_size);
                    break;
                case MARKED:
                    painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY,
                                       bmpBlocks, block_size * 11, 0, block_size, block_size);
                    break;
                case BOMB:
                    painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY,
                                       bmpBlocks, block_size * 9, 0, block_size, block_size);
                    break;
                case WRONG_MARK:
                    if(state == PLAYING || state == LOSE){
                        painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY,
                                           bmpBlocks, block_size * 11, 0, block_size, block_size);
                    } else if(state == OVER){
                        painter.drawPixmap(j * block_size + offsetX, i * block_size + offsetY + spaceY,
                                           bmpBlocks, block_size * 12, 0, block_size, block_size);
                    }
                    break;
            }
        }
    }

    this->handle(game);
}

void MineSweeper::handle(GameModel * game){
    GameState state = game->getState();
    switch (state) {
        case OVER:
            this->timer->stop();
            break;
        case WIN:
            this->timer->stop();
            break;
        default:
            break;
    }
}

void MineSweeper::mousePressEvent(QMouseEvent * event){
    int mapCol = this->game->getMapCol();
    int mapRow = this->game->getMapRow();
    UNUSED(mapRow);

    GameState state = this->game->getState();
/*
        do{
            static bool first_run = 0;
            if(!first_run){
            for(unsigned long i=0; i<static_cast<unsigned long>(mapRow); i++)
                for(unsigned long j=0; j<static_cast<unsigned long>(mapCol); j++)
                    if(this->game->getMap()[i][j].getValue()==-1){
                        this->game->mark(static_cast<int>(i), static_cast<int>(j));
                        update();
                    } else {
                        this->game->dig(static_cast<int>(i), static_cast<int>(j));
                        update();
                    }
                        first_run = 1;
            return;
            }
        }while(0);
*/
    int mouse_x = event->x();
    int mouse_y = event->y();
    if(mouse_y < spaceY + offsetY){
        if(mouse_x >= (mapCol * block_size + offsetX * 2) / 2 - 12
         && mouse_x <= (mapCol * block_size + offsetX * 2) / 2 - 12
         && mouse_y >= spaceY / 2
                && mouse_y	<= spaceY / 2 + 24){
            this->game->restartGame();
            this->timer->start(1000);
            timelabel->setText("Time: " + QString::number(this->game->getTimer()) + " s");
            update();
        }
    } else if (state!=OVER&&state!=WIN) {
        int px = mouse_x - offsetX;
        int py = mouse_y - offsetY - spaceY;
        int row = py / block_size;
        int col = px / block_size;
        const int mapRow = this->game->getMapRow();
        const int mapCol = this->game->getMapCol();
        if(row<0||row>=mapRow||col<0||col>=mapCol)
            return;
        switch(event->button()){
            case Qt::LeftButton:
                this->game->dig(row,col);
                update();
                break;
            case Qt::RightButton:
                this->game->mark(row,col);
                update();
                break;
            default:
                break;
        }
    }
}

void MineSweeper::onStartGameClicked(){
    this->game->restartGame();
    this->timelabel->setText("Time: " + QString::number(this->game->getTimer()) + " s");
    this->timer->start(1000);
    update();
}

void MineSweeper::updateTimer(){
    this->game->updateTimer();
    this->timelabel->setText("Time: " + QString::number(this->game->getTimer()) + " s");
}
