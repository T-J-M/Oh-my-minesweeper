#include <QtDebug>
#include "gameinittool.h"
#include "gamemodel.h"
#include <vector>
#include "model.h"
#include <cstdlib>
#include <ctime>

void GameInitTool::InitMineBlocks(BlockMap & map, int row, int col){
    for(int i = 0; i < row; i++){
        std::vector<MineBlock> lineBlocks;
        for(int j = 0; j < col; j++){
            MineBlock block;
            block.setState(INIT);
            block.setValue(0);
            lineBlocks.emplace_back(block);
        }
        map.emplace_back(lineBlocks);
    }
}

void GameInitTool::ArrangeMine(BlockMap & map, int row, int col, int totalNum){
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    int n = totalNum;
    while(n > 0){
        unsigned long p_row = static_cast<unsigned long>(std::rand() % row);
        unsigned long p_col = static_cast<unsigned long>(std::rand() % col);
        if(map[p_row][p_col].getValue() != -1){
            map[p_row][p_col].setValue(-1);
            n--;
            /*
            QString msg("Mine position: ("+QString::number(p_row)+","+QString::number(p_col)+")");
            qDebug(msg.toLatin1());
            */
        }
    }
}

void GameInitTool::CalBlockSurrond(BlockMap & map, int row, int col){
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            unsigned long map_i = static_cast<unsigned long>(i);
            unsigned long map_j = static_cast<unsigned long>(j);
            if(map[map_i][map_j].getValue()!=-1){
                for(int x = -1; x < 2; x++){
                    for(int y = -1; y < 2; y++){
                        if(i+x>=0
                         && i+x<row
                         && j+y>=0
                         && j+y<col
                         && -1==map[static_cast<unsigned long>(i+x)][static_cast<unsigned long>(j+y)].getValue()
                                && !(x==0 && y==0)){
                            int oldVal = map[map_i][map_j].getValue();
                            map[map_i][map_j].setValue(oldVal+1);
                        }
                    }
                }
            }
        }
    }
}
