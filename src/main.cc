#include "minesweeper.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MineSweeper window;
    window.show();

    return app.exec();
}
