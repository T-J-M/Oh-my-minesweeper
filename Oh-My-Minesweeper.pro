#-------------------------------------------------
#
# Project created by QtCreator 2018-09-18T00:17:06
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = Oh-My-Minesweeper
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++1y

INCLUDEPATH += include
HEADERS += include/gameinittool.h \
           include/gamemodel.h \
           include/minesweeper.h \
           include/model.h
FORMS += minesweeper.ui
SOURCES += src/gameinittool.cc \
           src/gamemodel.cc \
           src/main.cc \
           src/mineblock.cc \
           src/minesweeper.cc
RESOURCES += icons.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
